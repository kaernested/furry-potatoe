# furry-potatoe
The purpose of this project is to explain bash commands.

## Install dependancies
Start with running `yarn` in your terminal to install dependencies

## Adding a bash command
The bash examples are stored in the `include/js/bash.js` file as an array of objects


A example of filling the object, with two examples:
```javascript
{ //bash command
    command: "ls",
    desc: "list<br>Birtir lista af skrám sem eru í núverandi möppu",
    examples: [
        {
            code: "ls -a",
            desc: "birtir lista af öllum skrám, þar á meðal faldar skrár",
        },
        {
            code: "ls -lh",
            desc: "birtir listann með fleirri upplýsingum",
        }
    ]
}
```
>>>
* `command` is the basic bash command
* `desc` describes the command
* `examples` is an array of example objects
    * `code` is example of the command in use
    * `desc` is description of the example code
>>>

### RULES for adding bash commands
1. Each bash command needs to be a separate **object**
2. Each object needs to have `command`, `desc` and `examples`
    * `command` needs to be a **string**
    * `desc` needs to be a **string**
    * `examples` needs to be an **array** 
3. The **command** `desc` needs to be setup in a specific way
    * Start with writing what the command is shorthand for (example: `ls` stands for _list_)
    * Following that you need to add a `<br>` to break the description line to include a _localised_ description of the command.
4. To exclude command examples, then simply leave the `examples` as a **empty array**.

Here is a empty template for the bash object
```javascript
{ //bash command
    command: "",
    desc: "",
    examples: [
        {
            code: "",
            desc: "",
        }
    ]
}
```

## Doing a test
When you have added your code to the array in `bash.js`, then you should run a little test.
1. run `yarn test` in your terminal
2. it will run a Jest test on the code, to see if you followed the rules.
3. If everything passes, then you can commit your code to the project